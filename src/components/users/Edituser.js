import Axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';

const Edituser = () => {
    let History = useHistory();
    const {id} = useParams();
    const [user, setUser] = useState({
        name: "",
        username: "",
        email: "",
        phnno: "",
        website: ""

    });
    const { name, username, email, phnno, website } = user;
    const onInputChange = e => {
        console.log(e.target.value);
        setUser({ ...user, [e.target.name]: e.target.value });
    };

    useEffect(() => {
        loadUser();
    }, []);

    const onSubmit = async e => {
        e.preventDefault();
        await Axios.put(`http://localhost:3003/users/${id}`, user);
        History.push("/");

    };
    const loadUser = async () => {
        const result = await Axios.get(`http://localhost:3003/users/${id}`);
        // console.log(result);
        setUser(result.data);

    };
    return (

        <div className="container">
            <div className="w-75 mx-auto shadow px-4">
                <h2 className="text-center mb-4">Edit User</h2>
                <form onSubmit={e => onSubmit(e)}>
                    <form>
                        <div className="form-group">
                            <input type="text"
                                className="form-control form-control-lg"
                                placeholder="Enter Your Name"
                                name="name"
                                value={name}
                                onChange={e => onInputChange(e)} >
                            </input>
                        </div>
                        <div className="form-group ">
                            <input type="text"
                                className="form-control form-control-lg"
                                id="colFormLabel"
                                placeholder="Enter Your UserName"
                                name="username"
                                value={username}
                                onChange={e => onInputChange(e)} />
                        </div>
                        <div className="form-group ">
                            <input type="email"
                                className="form-control form-control-lg"
                                id="colFormLabel"
                                placeholder="Enter Your Email Address"
                                name="email"
                                value={email}
                                onChange={e => onInputChange(e)} />
                        </div>
                        <div className="form-group ">
                            <input type="phone"
                                className="form-control form-control-lg"
                                id="colFormLabel"
                                placeholder="Enter Your Phone Number"
                                name="phnno"
                                value={phnno}
                                onChange={e => onInputChange(e)} />
                        </div>
                        <div className="form-group ">
                            <input type="text"
                                className="form-control form-control-lg"
                                id="colFormLabel"
                                placeholder="Enter Your Website"
                                name="website"
                                value={website}
                                onChange={e => onInputChange(e)} />
                        </div>
                        <button className="btn btn-warning btn-block"  >Edit users</button>
                    </form>
                </form>
            </div>
        </div>
    );
};

export default Edituser;